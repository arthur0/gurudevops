---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

My name is Arthur Miranda. Things about me:

- I love being Luiz's father 
- I like to cook
- I'm a Dev, enthusiastic about DevOps and Microservices
- I'm continually learning 
- I'm not a Guru, but the *gurudevops* domain was available in a good price
  -  So I think I have to become one or buy another domain
- Feedback is always welcome


### At Work

I worked as Dev in different projects, including developing mobile and web 
apps, crating and managing infrastructure running on different cloud providers, 
and OpenSource communities. 
Now, I've been working/learning with cloud-native applications.

About tech stack, currently I like (I have some experience with):

- Kubernetes
- Docker
- Python, especially REST APIs with Flask 
- Google Cloud Platform
- Golang
- MongoDB
- Redis
- Prometheus/Grafana
- CI (GiLabCI and CircleCI, mainly)
- GitLab
- Linux
- The backend side of the force

And 'dislike' (I'm dumb with - but I think it is a cool stuff):

- Java-Script Frameworks 
- UX things
- The frontend side of the force

Here is a photo of the 9 months old Luiz after eating mango:
![](/luiz.jpg)
He is the only project whose frontend I am proud to have contributed

See ya.